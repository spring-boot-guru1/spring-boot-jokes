package br.com.mrb.jokesapp.service;

import org.springframework.stereotype.Service;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;

@Service
public class JokeServiceIml implements JokeService{

	private final ChuckNorrisQuotes chuckNorrisQuotes;
	
	public JokeServiceIml() {
		this.chuckNorrisQuotes = new ChuckNorrisQuotes();
	}
	
	
	@Override
	public String getJoke() {
		// TODO Auto-generated method stub
		return this.chuckNorrisQuotes.getRandomQuote();
	}

}
